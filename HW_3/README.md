Можно сгенерировать себе профиль по OpenAPI спецификации:

npx @openapitools/openapi-generator-cli generate -i http://weather-api.romko11l/swagger/v1/swagger.json -g k6 -o ./k6-test/

# Методика экспериментов

Понятно, что на задержку ответа от приложения будет влиять количество данных в таблицах `cities` и `forecast`, наличие индексов / партиционирования.
Учитывая это, довольно сложно построить отражающий реальность тест (профиль нагрузки).
Например, если мы будем безгранично создавать новые записи в `forecast`, то в какой-то момент запрос `GET /WeatherForecast` будет выкачивать огромный json и не уложится ни в какие лимиты.

Для простоты мы сделаем комплексный профиль с заполнением базы `populate.js` и два профиля с идемпотентными `Get` запросами `idempotent_load_simple.js` (здесь тестируются запросы, на которые ответ - маленький json) и `idempotent_load_hard.js` (здесь тестируются запросы, на которые в качестве ответа приходит большой json), на которых и будем мерить производительность.

## `populate.js`

```bash
k6 run ./populate.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: ./populate.js
     output: -

  scenarios: (100.00%) 1 scenario, 25 max VUs, 4m0s max duration (incl. graceful stop):
           * contacts: Up to 25 looping VUs for 3m30s over 3 stages (gracefulRampDown: 30s, gracefulStop: 30s)


     █ /Forecast/{cityId}

       ✗ Success
        ↳  99% — ✓ 1241 / ✗ 3

     █ /WeatherForecast

       ✓ Success

     █ /Forecast

       ✓ Success

     █ /Forecast/{id}

       ✗ Success
        ↳  99% — ✓ 2482 / ✗ 6

     █ /Cities/{id}

       ✗ Success
        ↳  99% — ✓ 2483 / ✗ 5

     █ /Cities

       ✓ Success

     checks.........................: 99.87% ✓ 11182     ✗ 14   
     data_received..................: 166 MB 776 kB/s
     data_sent......................: 1.7 MB 8.0 kB/s
     group_duration.................: avg=422.75ms min=5.85ms  med=22.09ms max=7.1s     p(90)=1.78s    p(95)=3.42s  
     http_req_blocked...............: avg=90.8µs   min=950ns   med=3.15µs  max=114.39ms p(90)=7.94µs   p(95)=9.54µs 
     http_req_connecting............: avg=85.65µs  min=0s      med=0s      max=114.31ms p(90)=0s       p(95)=0s     
     http_req_duration..............: avg=281.57ms min=5.02ms  med=11.37ms max=7.1s     p(90)=235.03ms p(95)=2.51s  
       { expected_response:true }...: avg=281.91ms min=5.02ms  med=11.38ms max=7.1s     p(90)=235.59ms p(95)=2.51s  
     http_req_failed................: 0.12%  ✓ 14        ✗ 11182
     http_req_receiving.............: avg=2.36ms   min=11.13µs med=81.44µs max=349.91ms p(90)=7.89ms   p(95)=13.95ms
     http_req_sending...............: avg=24.25µs  min=4.53µs  med=18.03µs max=6.01ms   p(90)=46.17µs  p(95)=53.2µs 
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s       p(95)=0s     
     http_req_waiting...............: avg=279.18ms min=4.8ms   med=10.19ms max=7.08s    p(90)=229.59ms p(95)=2.49s  
     http_reqs......................: 11196  52.372014/s
     iteration_duration.............: avg=2.53s    min=67.99ms med=2.39s   max=7.27s    p(90)=5.09s    p(95)=5.85s  
     iterations.....................: 1244   5.819113/s
     vus............................: 6      min=3       max=24 
     vus_max........................: 25     min=25      max=25 


running (3m33.8s), 00/25 VUs, 1244 complete and 0 interrupted iterations
contacts ✓ [======================================] 00/25 VUs  3m30s
```

## `idempotent_load_simple.js`

```bash
k6 run ./idempotent_load_simple.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: ./idempotent_load_simple.js
     output: -

  scenarios: (100.00%) 1 scenario, 50 max VUs, 15m30s max duration (incl. graceful stop):
           * contacts: Up to 50 looping VUs for 15m0s over 1 stages (gracefulRampDown: 30s, gracefulStop: 30s)

WARN[0099] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0099] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0211] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0362] Request Failed                                error="Get \"http://weather-api.romko11l/Cities/1\": dial: i/o timeout"
WARN[0389] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0389] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0389] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0467] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0468] Request Failed                                error="Get \"http://weather-api.romko11l/Cities/1\": dial: i/o timeout"
WARN[0539] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0539] Request Failed                                error="Get \"http://weather-api.romko11l/Forecast/1\": dial: i/o timeout"
WARN[0676] Request Failed                                error="Get \"http://weather-api.romko11l/Cities/1\": dial: i/o timeout"

     █ /Forecast/{id}

       ✗ Success
        ↳  99% — ✓ 677310 / ✗ 9

     █ /Cities/{id}

       ✗ Success
        ↳  99% — ✓ 677316 / ✗ 3

     checks.........................: 99.99%  ✓ 1354626     ✗ 12     
     data_received..................: 288 MB  320 kB/s
     data_sent......................: 129 MB  143 kB/s
     group_duration.................: avg=17.24ms  min=4.57ms med=8.12ms  max=30s      p(90)=54.56ms  p(95)=69.69ms 
     http_req_blocked...............: avg=88.58µs  min=0s     med=1.83µs  max=15.44s   p(90)=5.7µs    p(95)=7.07µs  
     http_req_connecting............: avg=85.31µs  min=0s     med=0s      max=15.44s   p(90)=0s       p(95)=0s      
     http_req_duration..............: avg=16.8ms   min=0s     med=8ms     max=737.3ms  p(90)=54.35ms  p(95)=69.46ms 
       { expected_response:true }...: avg=16.8ms   min=4.52ms med=8ms     max=737.3ms  p(90)=54.35ms  p(95)=69.46ms 
     http_req_failed................: 0.00%   ✓ 12          ✗ 1354626
     http_req_receiving.............: avg=153.17µs min=0s     med=32.97µs max=720.24ms p(90)=121.71µs p(95)=576.46µs
     http_req_sending...............: avg=12.57µs  min=0s     med=8.64µs  max=19.36ms  p(90)=26.77µs  p(95)=31.84µs 
     http_req_tls_handshaking.......: avg=0s       min=0s     med=0s      max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=16.63ms  min=0s     med=7.86ms  max=661.97ms p(90)=54.13ms  p(95)=69.25ms 
     http_reqs......................: 1354638 1505.112572/s
     iteration_duration.............: avg=34.53ms  min=10.2ms med=18.26ms max=30.03s   p(90)=79.89ms  p(95)=87.83ms 
     iterations.....................: 677319  752.556286/s
     vus............................: 49      min=3         max=49   
     vus_max........................: 50      min=50        max=50   


running (15m00.0s), 00/50 VUs, 677319 complete and 0 interrupted iterations
contacts ✓ [======================================] 00/50 VUs  15m0s
```

![rps simple](./images/rps_simple.png)

![cpu simple](./images/cpu_simple.png)

Скорее всего в момент ~14:03 тест упёрся в возможности локальной машины, с которой отправляются запросы. Тем не менее удалось вплотную приблизиться к максимальной загрузке сервиса (судя по графику `CPU Usage` и тому, что редкие запросы всё-таки фейлились).

## `idempotent_load_hard.js`

```bash
k6 run ./idempotent_load_hard.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: ./idempotent_load_hard.js
     output: -

  scenarios: (100.00%) 1 scenario, 50 max VUs, 15m30s max duration (incl. graceful stop):
           * contacts: Up to 50 looping VUs for 15m0s over 1 stages (gracefulRampDown: 30s, gracefulStop: 30s)


     █ /WeatherForecast

       ✓ Success

     █ /Forecast

       ✓ Success

     █ /Cities

       ✓ Success

     checks.........................: 100.00% ✓ 6555     ✗ 0   
     data_received..................: 579 MB  635 kB/s
     data_sent......................: 627 kB  688 B/s
     group_duration.................: avg=3.62s    min=10.29ms med=37.15ms max=39.19s   p(90)=14.38s  p(95)=17.99s 
     http_req_blocked...............: avg=110.36µs min=1.24µs  med=5.41µs  max=13.3ms   p(90)=9.08µs  p(95)=10.13µs
     http_req_connecting............: avg=103.32µs min=0s      med=0s      max=13.16ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=3.61s    min=10.13ms med=36.86ms max=39.19s   p(90)=14.38s  p(95)=17.99s 
       { expected_response:true }...: avg=3.61s    min=10.13ms med=36.86ms max=39.19s   p(90)=14.38s  p(95)=17.99s 
     http_req_failed................: 0.00%   ✓ 0        ✗ 6555
     http_req_receiving.............: avg=12.87ms  min=64.44µs med=8.51ms  max=362.46ms p(90)=23.76ms p(95)=26.98ms
     http_req_sending...............: avg=29.22µs  min=5.24µs  med=29.03µs max=569.21µs p(90)=46.55µs p(95)=51.18µs
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=3.6s     min=7.13ms  med=27.98ms max=39.17s   p(90)=14.35s  p(95)=17.96s 
     http_reqs......................: 6555    7.192771/s
     iteration_duration.............: avg=10.86s   min=2.13s   med=9.39s   max=39.23s   p(90)=19.87s  p(95)=22.77s 
     iterations.....................: 2185    2.39759/s
     vus............................: 1       min=1      max=49
     vus_max........................: 50      min=50     max=50


running (15m11.3s), 00/50 VUs, 2185 complete and 0 interrupted iterations
contacts ✓ [======================================] 00/50 VUs  15m0s
```

![rps hard](./images/rps_hard.png)

![cpu hard](./images/cpu_hard.png)

Как легко догадаться - на тяжёлых запросах упёрлись в базу. Посмотрим на график cpu мастера постгреса:

![cpu hard](./images/cpu_postgres.png)

Очевидно, базе трудно формировать огромные ответы.

# Выводы

1. При запросах на чтение база выдерживает ~2k rps легковесных запросов и ~7 rps на тяжеловесных (понятно, что второе сильно зависит от числа записей в базе).

2. На тяжеловесные запросы нужно вводить серьёзный `ratelimit`.

# Возможные SLI / SLO для сервиса

Разделим все хендлеры API на три группы и для каждой группы зададим SLO

1. Хендлеры обновления данных
    - Количество ошибок сервиса не превышает 0.5% от общего числа запросов.
    - Время ответа не превышает 0.1 с в 99% случаев
2. Легковесные хендлеры получения данных
    - Количество ошибок сервиса не превышает 0.1% от общего числа запросов.
    - Время ответа не превышает 0.1 с в 95% случаев
3. Тжеловесные хендлеры получения данных
    - Количество ошибок сервиса не превышает 0.1% от общего числа запросов.
    - Время ответа не превышает 20 с в 95% случаев
